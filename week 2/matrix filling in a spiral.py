"""
Выведите таблицу размером n×n, заполненную числами от 1 до n^2
по спирали, выходящей из левого верхнего угла и закрученной по часовой стрелке, как показано в примере (здесь n=5):
Sample Input:
5
Sample Output:
1 2 3 4 5
16 17 18 19 6
15 24 25 20 7
14 23 22 21 8
13 12 11 10 9
"""
n = int(input())
matrix = [[0] * n for i in range(n)]
number = 1
m = 0
matrix[n // 2][n // 2] = n * n
for i in range(n // 2):
    # Заполнение верхней горизонтальной матрицы
    for j in range(n - m):
        matrix[i][j + i] = number
        number += 1
    # Заполнение правой вертикальной матрицы
    for j in range(i + 1, n - i):
        matrix[j][-i - 1] = number
        number += 1
    # Заполнение нижней горизонтальной матрицы
    for j in range(i + 1, n - i):
        matrix[-i - 1][-j - 1] = number
        number += 1
    # Заполнение левой вертикальной матрицы
    for j in range(i + 1, n - (i + 1)):
        matrix[-j - 1][i] = number
        number += 1

    m += 2
for i in matrix:
    for j in i:
        print(j, end=' ')
    print()
