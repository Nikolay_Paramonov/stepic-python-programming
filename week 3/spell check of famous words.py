"""
Простейшая система проверки орфографии может быть основана на использовании списка известных слов.
Если введённое слово не найдено в этом списке, оно помечается как "ошибка".
Попробуем написать подобную систему.
На вход программе первой строкой передаётся количество dd известных нам слов, после чего на dd строках указываются
эти слова. Затем передаётся количество ll строк текста для проверки, после чего ll строк текста.
Выведите уникальные "ошибки" в произвольном порядке. Работу производите без учёта регистра.
Sample Input:
4
champions
we
are
Stepik
3
We are the champignons
We Are The Champions
Stepic
Sample Output:
stepic
champignons
the
"""
words = []
errors = []
number = int(input())
for i in range(number):
    word = input()
    words.append(word.lower())
count_sentence = int(input())
for j in range(count_sentence):
    sentence = input().lower().split()
    for i in sentence:
        if i not in words:
            errors.append(i)
for i in set(errors):
    print(i)
