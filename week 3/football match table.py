"""
Напишите программу, которая принимает на стандартный вход список игр футбольных команд с результатом матча и выводит
на стандартный вывод сводную таблицу результатов всех матчей.
За победу команде начисляется 3 очка, за поражение — 0, за ничью — 1.
Формат ввода следующий:
В первой строке указано целое число nn — количество завершенных игр.
После этого идет nn строк, в которых записаны результаты игры в следующем формате:
Первая_команда;Забито_первой_командой;Вторая_команда;Забито_второй_командой
Вывод программы необходимо оформить следующим образом:
Команда:Всего_игр Побед Ничьих Поражений Всего_очков
Конкретный пример ввода-вывода приведён ниже.
Порядок вывода команд произвольный.
Sample Input:
3
Спартак;9;Зенит;10
Локомотив;12;Зенит;3
Спартак;8;Локомотив;15
Sample Output:
Спартак:2 0 0 2 0
Зенит:2 1 0 1 3
Локомотив:2 2 0 0 6
"""
n = int(input())
input_list = [input().split(';') for j in range(n)]
rivals = []
teams = []
for i in input_list:
    if i not in rivals:
        rivals.append(i[0])
        rivals.append(i[2])
for j in rivals:
    if j not in teams:
        teams.append(j)
results = {team: [0, 0, 0, 0, 0]for team in teams}
for team1, gol1, team2, gol2 in input_list:
    results[team1][0] += 1
    results[team2][0] += 1
    if int(gol1) > int(gol2):
        results[team1][1] += 1
        results[team1][4] += 3
        results[team2][3] += 1
    elif int(gol1) < int(gol2):
        results[team2][1] += 1
        results[team2][4] += 3
        results[team1][3] += 1
    elif int(gol1) == int(gol2):
        results[team1][2] += 1
        results[team1][4] += 1
        results[team2][2] += 1
        results[team2][4] += 1
for team in teams:
    print('{}:{}'.format(team, ' '.join(map(str, results[team]))))
