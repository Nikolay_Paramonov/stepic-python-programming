"""
Имеется набор файлов, каждый из которых, кроме последнего, содержит имя следующего файла.
Первое слово в тексте последнего файла: "We".
Скачайте предложенный файл. В нём содержится ссылка на первый файл из этого набора.
Все файлы располагаются в каталоге по адресу:
https://stepic.org/media/attachments/course67/3.6.3/
Загрузите содержимое ﻿последнего файла из набора, как ответ на это задание.
"""
import requests
with open('/home/nikolay/Загрузки/dataset_3378_3.txt') as inf:
    r = inf.readline().strip()
url = 'https://stepic.org/media/attachments/course67/3.6.3/'
r = str(requests.get(r).text)
cnt = 0
while 'We' not in r:
    cnt += 1
    print(cnt, r)
    r = requests.get(url + r).text
print(r)
